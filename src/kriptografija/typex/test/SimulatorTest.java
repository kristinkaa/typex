package kriptografija.typex.test;

import kriptografija.typex.Pretvorba;
import kriptografija.typex.Rotor;
import kriptografija.typex.Simulator;

public class SimulatorTest {
	
	private static void simuliraj(String lr, String sr, String dr, String ls, String ds, boolean lri, boolean sri,
			                      boolean dri, boolean lsi, boolean dsi, String pocetno, String ulaz)
	{
		Rotor DS = new Rotor(Pretvorba.stringUArray(ds), pocetno.charAt(4) - 'A', dsi);
		Rotor LS = new Rotor(Pretvorba.stringUArray(ls), pocetno.charAt(3) - 'A', lsi);
		Rotor DR = new Rotor(Pretvorba.stringUArray(dr), pocetno.charAt(2) - 'A', dri);
		Rotor SR = new Rotor(Pretvorba.stringUArray(sr), pocetno.charAt(1) - 'A', sri);
		Rotor LR = new Rotor(Pretvorba.stringUArray(lr), pocetno.charAt(0) - 'A', lri);
		Rotor REF = new Rotor(Pretvorba.stringUArray(Simulator.REF), 0, false);
		
		Simulator sim = new Simulator(DS, LS, DR, SR, LR, REF, Simulator.ZAREZ, "AA");
		System.out.println(sim.simulator(ulaz));
	}

	public static void main(String[] args) {
		
		simuliraj(Simulator.R1, Simulator.R2, Simulator.R3, Simulator.R4, Simulator.R5, false, false, false, false, false, "AAAAA", "AA");
	}

}
