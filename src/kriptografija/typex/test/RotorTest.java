package kriptografija.typex.test;

import kriptografija.typex.Pretvorba;
import kriptografija.typex.Rotor;
import kriptografija.typex.Simulator;

public class RotorTest {

	public static void main(String[] args) {
		
		Rotor r = new Rotor(Pretvorba.stringUArray(Simulator.R1), 1, true);
		for (int i = 0; i < 26; i++)
			System.out.print((char)(r.dohvatiKod(i)+'A'));
		System.out.println();
		r.rotiraj();
		for (int i = 0; i < 26; i++)
			System.out.print((char)(r.dohvatiKod(i)+'A'));
		
		int[] polje = {0, 3, 2, 4, 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
		Rotor rotor = new Rotor(polje, 0, false);
		System.out.println(rotor.dohvatiKod(0));

	}
}
