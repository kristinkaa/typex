package kriptografija.typex;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.GridLayout;

import javax.swing.SwingConstants;
import javax.swing.JTextPane;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Panel;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.JMenu;

import java.awt.Dimension;

import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.UIManager;

public class GlavniEkran extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldUlaz;
	private JTextField textFieldPocetnoStanje;
	public JTextField textFieldPrvi;
	public JTextField textFieldDrugi;
	public JTextField textFieldTreci;
	public JTextField textFieldCetvrti;
	public JTextField textFieldPeti;
	public JTextField textFieldSesti;
	public JTextField textFieldSedmi;
	public JTextField textFieldOsmi;
	public JTextField textFieldDeveti;
	public JTextField textFieldDeseti;
	public JTextField textFieldJedanaesti;
	private JSeparator separator_1;
	public JTextField textFieldIzlaz;
	private JLabel labelStrelica4;
	private JLabel labelStrelica3;
	private Simulator sim = null;
	private JLabel lblDs;
	private JLabel lblLs;
	private JLabel lblDr;
	private JLabel lblSr;
	private JLabel lblLr;
	private JLabel lblRef;
	private JLabel lblLri;
	private JLabel lblSri;
	private JLabel lblDri;
	private JLabel lblLsi;
	private JLabel lblDsi;
	private JLabel label;
	private JLabel label_1;
	private JSeparator separator_2;
	private JSeparator separator_3;
	private JMenuItem mntmOProgramu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GlavniEkran frame = new GlavniEkran();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GlavniEkran() {
	
		setResizable(false);
		setFont(new Font("Tahoma", Font.PLAIN, 12));
		setTitle("Typex simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 397);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBorder(UIManager.getBorder("MenuBar.border"));
		menuBar.setMinimumSize(new Dimension(2, 2));
		menuBar.setMaximumSize(new Dimension(2, 2));
		setJMenuBar(menuBar);
		
		JMenu mnIzbornik = new JMenu("Izbornik");
		mnIzbornik.setBorder(UIManager.getBorder("MenuItem.border"));
		mnIzbornik.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menuBar.add(mnIzbornik);
		
		
		
		JMenuItem mntmRotori = new JMenuItem("Rotori", KeyEvent.VK_T);
		mntmRotori.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(GlavniEkran.this, "Stanja rotora:\n" +
						"LR = \"QWECYJIBFKMLTVZPOHUDGNRSXA\"; SR = \"AJDKSIRUXBLHWTMCQGZNPYFVOE\";\n" +
						"DR = \"BDFHJLCPRTXVZNYEIWGAKMUSQO\"; LS = \"ESOVPZJAYQUIRHXLNFTGKDCMWB\";\n" +
						"DS = \"VZBRGITYUPSDNHLXAWMJQOFECK\";\n" +
						"REF = \"YRUHQSLDPXNGOKMIEBFZCWVJAT\"; ZAREZ = \"ACEINQTVY\";\n\n" +
						"Za još više detalja pogledajte pripadajuću dokumentaciju.\n\n", "Detalji o rotorima", JOptionPane.PLAIN_MESSAGE);
			}
		});
		
		final JButton btnKorak = new JButton("Korak");
		final JButton btnMag = new JButton("Magic");
		
		JMenuItem mntmNovo = new JMenuItem("Novo", KeyEvent.VK_T);
		mntmNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFieldUlaz.setText(null);
				textFieldUlaz.setEnabled(true);
				textFieldPocetnoStanje.setText(null);
				textFieldPocetnoStanje.setEnabled(true);
				textFieldIzlaz.setText(null);
				btnKorak.setEnabled(true);
				btnMag.setEnabled(true);
				sim = null;
				
			}
		});
		mntmNovo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		mntmNovo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		mnIzbornik.add(mntmNovo);
		mntmRotori.setFont(new Font("Tahoma", Font.PLAIN, 12));
		mntmRotori.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
		mnIzbornik.add(mntmRotori);
		
		separator_2 = new JSeparator(SwingConstants.VERTICAL);
		separator_2.setMaximumSize(new Dimension(1, 100));
		separator_2.setPreferredSize(new Dimension(1, 1));
		menuBar.add(separator_2);
		
		
		JMenu mnOProgramu = new JMenu("Pomoć");
		mnOProgramu.setBorder(UIManager.getBorder("MenuItem.border"));
		mnOProgramu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menuBar.add(mnOProgramu);
		
		mntmOProgramu = new JMenuItem("O programu");
		mntmOProgramu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		mntmOProgramu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(GlavniEkran.this, "Copyright © Kristina Marinić", "O programu", JOptionPane.PLAIN_MESSAGE);

			}
		});
		mnOProgramu.add(mntmOProgramu);
		
		separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		menuBar.add(separator_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel donjiPanel = new JPanel();
		contentPane.add(donjiPanel, BorderLayout.SOUTH);
		donjiPanel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel velikiPanel = new JPanel();
		contentPane.add(velikiPanel, BorderLayout.CENTER);
		velikiPanel.setLayout(null);
		
		textFieldUlaz = new JTextField();
		textFieldUlaz.setText("");
		textFieldUlaz.setBounds(81, 24, 150, 20);
		velikiPanel.add(textFieldUlaz);
		textFieldUlaz.setColumns(10);
		
		label = new JLabel("->");
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setBounds(501, 98, 30, 14);
		velikiPanel.add(label);
		
		label_1 = new JLabel("<-");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(499, 218, 30, 14);
		velikiPanel.add(label_1);
		
		lblDsi = new JLabel("DSI");
		lblDsi.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDsi.setHorizontalAlignment(SwingConstants.CENTER);
		lblDsi.setBounds(36, 178, 46, 14);
		velikiPanel.add(lblDsi);
		
		lblLsi = new JLabel("LSI");
		lblLsi.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblLsi.setHorizontalAlignment(SwingConstants.CENTER);
		lblLsi.setBounds(136, 178, 46, 14);
		velikiPanel.add(lblLsi);
		
		lblDri = new JLabel("DRI");
		lblDri.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDri.setHorizontalAlignment(SwingConstants.CENTER);
		lblDri.setBounds(236, 178, 46, 14);
		velikiPanel.add(lblDri);
		
		lblSri = new JLabel("SRI");
		lblSri.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblSri.setHorizontalAlignment(SwingConstants.CENTER);
		lblSri.setBounds(336, 178, 46, 14);
		velikiPanel.add(lblSri);
		
		lblLri = new JLabel("LRI");
		lblLri.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblLri.setHorizontalAlignment(SwingConstants.CENTER);
		lblLri.setBounds(436, 178, 46, 14);
		velikiPanel.add(lblLri);
		
		lblRef = new JLabel("REF");
		lblRef.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblRef.setHorizontalAlignment(SwingConstants.CENTER);
		lblRef.setBounds(464, 162, 46, 14);
		velikiPanel.add(lblRef);
		
		lblLr = new JLabel("LR");
		lblLr.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblLr.setHorizontalAlignment(SwingConstants.CENTER);
		lblLr.setBounds(436, 143, 46, 14);
		velikiPanel.add(lblLr);
		
		lblDr = new JLabel("DR");
		lblDr.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDr.setHorizontalAlignment(SwingConstants.CENTER);
		lblDr.setBounds(236, 143, 46, 14);
		velikiPanel.add(lblDr);
		
		lblLs = new JLabel("LS");
		lblLs.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblLs.setHorizontalAlignment(SwingConstants.CENTER);
		lblLs.setBounds(136, 143, 46, 14);
		velikiPanel.add(lblLs);
		
		JLabel labelStrelica8 = new JLabel("<-");
		labelStrelica8.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica8.setBounds(92, 218, 30, 14);
		velikiPanel.add(labelStrelica8);
		
		JLabel labelStrelica6 = new JLabel("<-");
		labelStrelica6.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica6.setBounds(292, 218, 30, 14);
		velikiPanel.add(labelStrelica6);
		
		JLabel labelStrelica7 = new JLabel("<-");
		labelStrelica7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica7.setBounds(192, 218, 30, 14);
		velikiPanel.add(labelStrelica7);
		
		labelStrelica3 = new JLabel("->");
		labelStrelica3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica3.setBounds(403, 98, 30, 14);
		velikiPanel.add(labelStrelica3);
		
		labelStrelica4 = new JLabel("->");
		labelStrelica4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica4.setBounds(303, 98, 30, 14);
		velikiPanel.add(labelStrelica4);
		
		JLabel lblUlaz = new JLabel("Ulaz:");
		lblUlaz.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUlaz.setBounds(52, 27, 30, 14);
		velikiPanel.add(lblUlaz);
		
		JLabel lblPoetnoStanje = new JLabel("Početno stanje:");
		lblPoetnoStanje.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPoetnoStanje.setBounds(277, 26, 90, 14);
		velikiPanel.add(lblPoetnoStanje);
		
		textFieldPocetnoStanje = new JTextField();
		textFieldPocetnoStanje.setBounds(369, 24, 150, 20);
		velikiPanel.add(textFieldPocetnoStanje);
		textFieldPocetnoStanje.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 69, 552, 2);
		velikiPanel.add(separator);
		
		textFieldPrvi = new JTextField();
		textFieldPrvi.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldPrvi.setEditable(false);
		textFieldPrvi.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldPrvi.setBounds(32, 82, 50, 50);
		velikiPanel.add(textFieldPrvi);
		textFieldPrvi.setColumns(10);
		
		textFieldDrugi = new JTextField();
		textFieldDrugi.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldDrugi.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldDrugi.setEditable(false);
		textFieldDrugi.setColumns(10);
		textFieldDrugi.setBounds(132, 82, 50, 50);
		velikiPanel.add(textFieldDrugi);
		
		textFieldTreci = new JTextField();
		textFieldTreci.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldTreci.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldTreci.setEditable(false);
		textFieldTreci.setColumns(10);
		textFieldTreci.setBounds(232, 82, 50, 50);
		velikiPanel.add(textFieldTreci);
		
		textFieldCetvrti = new JTextField();
		textFieldCetvrti.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldCetvrti.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldCetvrti.setEditable(false);
		textFieldCetvrti.setColumns(10);
		textFieldCetvrti.setBounds(332, 82, 50, 50);
		velikiPanel.add(textFieldCetvrti);
		
		textFieldPeti = new JTextField();
		textFieldPeti.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldPeti.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldPeti.setEditable(false);
		textFieldPeti.setColumns(10);
		textFieldPeti.setBounds(432, 82, 50, 50);
		velikiPanel.add(textFieldPeti);
		
		textFieldSesti = new JTextField();
		textFieldSesti.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldSesti.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldSesti.setEditable(false);
		textFieldSesti.setColumns(10);
		textFieldSesti.setBounds(512, 142, 50, 50);
		velikiPanel.add(textFieldSesti);
		
		textFieldSedmi = new JTextField();
		textFieldSedmi.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldSedmi.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldSedmi.setEditable(false);
		textFieldSedmi.setColumns(10);
		textFieldSedmi.setBounds(432, 202, 50, 50);
		velikiPanel.add(textFieldSedmi);
		
		textFieldOsmi = new JTextField();
		textFieldOsmi.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldOsmi.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldOsmi.setEditable(false);
		textFieldOsmi.setColumns(10);
		textFieldOsmi.setBounds(332, 202, 50, 50);
		velikiPanel.add(textFieldOsmi);
		
		textFieldDeveti = new JTextField();
		textFieldDeveti.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldDeveti.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldDeveti.setEditable(false);
		textFieldDeveti.setColumns(10);
		textFieldDeveti.setBounds(232, 202, 50, 50);
		velikiPanel.add(textFieldDeveti);
		
		textFieldDeseti = new JTextField();
		textFieldDeseti.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldDeseti.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldDeseti.setEditable(false);
		textFieldDeseti.setColumns(10);
		textFieldDeseti.setBounds(132, 202, 50, 50);
		velikiPanel.add(textFieldDeseti);
		
		textFieldJedanaesti = new JTextField();
		textFieldJedanaesti.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldJedanaesti.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textFieldJedanaesti.setEditable(false);
		textFieldJedanaesti.setColumns(10);
		textFieldJedanaesti.setBounds(32, 202, 50, 50);
		velikiPanel.add(textFieldJedanaesti);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(12, 265, 552, 2);
		velikiPanel.add(separator_1);
		
		textFieldIzlaz = new JTextField();
		textFieldIzlaz.setBounds(202, 293, 150, 20);
		velikiPanel.add(textFieldIzlaz);
		textFieldIzlaz.setColumns(10);
		
		JLabel lblIzlaz = new JLabel("Izlaz:");
		lblIzlaz.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblIzlaz.setBounds(172, 295, 30, 14);
		velikiPanel.add(lblIzlaz);
		
		JLabel labelStrelica = new JLabel("->");
		labelStrelica.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica.setBounds(102, 98, 30, 14);
		velikiPanel.add(labelStrelica);
		
		JLabel labelStrelica2 = new JLabel("->");
		labelStrelica2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica2.setBounds(201, 98, 30, 14);
		velikiPanel.add(labelStrelica2);
		
		JLabel labelStrelica5 = new JLabel("<-");
		labelStrelica5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelStrelica5.setBounds(392, 218, 30, 14);
		velikiPanel.add(labelStrelica5);
		

		
		lblDs = new JLabel("DS");
		lblDs.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDs.setHorizontalAlignment(SwingConstants.CENTER);
		lblDs.setBounds(36, 143, 46, 14);
		velikiPanel.add(lblDs);
		
		lblSr = new JLabel("SR");
		lblSr.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblSr.setHorizontalAlignment(SwingConstants.CENTER);
		lblSr.setBounds(336, 143, 46, 14);
		velikiPanel.add(lblSr);
		
		
		btnKorak.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnKorak.setBounds(384, 291, 75, 23);
		velikiPanel.add(btnKorak);
		
		
		btnMag.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnMag.setBounds(479, 291, 75, 23);
		velikiPanel.add(btnMag);
		btnMag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFieldPocetnoStanje.setText(textFieldPocetnoStanje.getText().toUpperCase());
				textFieldUlaz.setText(textFieldUlaz.getText().toUpperCase());
				btnKorak.setEnabled(false);
				if (textFieldPocetnoStanje.getText().length() != 5)
				{
					JOptionPane.showMessageDialog(GlavniEkran.this, "Početno stanje mora imati pet slova!", "Pogreska", JOptionPane.PLAIN_MESSAGE);
					return;
				}
				textFieldUlaz.setEnabled(false);
				textFieldPocetnoStanje.setEnabled(false);
				Rotor DS = new Rotor(Pretvorba.stringUArray(Simulator.R5), textFieldPocetnoStanje.getText().charAt(4) - 'A', false);
				Rotor LS = new Rotor(Pretvorba.stringUArray(Simulator.R4), textFieldPocetnoStanje.getText().charAt(3) - 'A', false);
				Rotor DR = new Rotor(Pretvorba.stringUArray(Simulator.R3), textFieldPocetnoStanje.getText().charAt(2) - 'A', false);
				Rotor SR = new Rotor(Pretvorba.stringUArray(Simulator.R2), textFieldPocetnoStanje.getText().charAt(1) - 'A', false);
				Rotor LR = new Rotor(Pretvorba.stringUArray(Simulator.R1), textFieldPocetnoStanje.getText().charAt(0) - 'A', false);
				Rotor REF = new Rotor(Pretvorba.stringUArray(Simulator.REF), 0, false);
				Simulator sim = new Simulator(DS, LS, DR, SR, LR, REF, Simulator.ZAREZ, textFieldUlaz.getText());
				textFieldIzlaz.setText(sim.simulator(textFieldUlaz.getText()));
			}
		});
		btnKorak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (sim == null)
				{
					textFieldPocetnoStanje.setText(textFieldPocetnoStanje.getText().toUpperCase());
					textFieldUlaz.setText(textFieldUlaz.getText().toUpperCase());
					if (textFieldPocetnoStanje.getText().length() != 5)
					{
						JOptionPane.showMessageDialog(GlavniEkran.this, "Početno stanje mora imati pet slova!", "Pogreska", JOptionPane.PLAIN_MESSAGE);
						return;
					}
					textFieldUlaz.setEnabled(false);
					textFieldPocetnoStanje.setEnabled(false);
					Rotor DS = new Rotor(Pretvorba.stringUArray(Simulator.R5), textFieldPocetnoStanje.getText().charAt(4) - 'A', false);
					Rotor LS = new Rotor(Pretvorba.stringUArray(Simulator.R4), textFieldPocetnoStanje.getText().charAt(3) - 'A', false);
					Rotor DR = new Rotor(Pretvorba.stringUArray(Simulator.R3), textFieldPocetnoStanje.getText().charAt(2) - 'A', false);
					Rotor SR = new Rotor(Pretvorba.stringUArray(Simulator.R2), textFieldPocetnoStanje.getText().charAt(1) - 'A', false);
					Rotor LR = new Rotor(Pretvorba.stringUArray(Simulator.R1), textFieldPocetnoStanje.getText().charAt(0) - 'A', false);
					Rotor REF = new Rotor(Pretvorba.stringUArray(Simulator.REF), 0, false);
					sim = new Simulator(DS, LS, DR, SR, LR, REF, Simulator.ZAREZ, textFieldUlaz.getText());
				}
				try {
					sim.korak(GlavniEkran.this);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(GlavniEkran.this, "Nema više koraka!", "Pogreska", JOptionPane.PLAIN_MESSAGE);
				}
			}
		});
	}
}
