package kriptografija.typex;

public class Simulator {
		
	public static String R1 = "QWECYJIBFKMLTVZPOHUDGNRSXA";
	public static String R2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
	public static String R3 = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
	public static String R4 = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
	public static String R5 = "VZBRGITYUPSDNHLXAWMJQOFECK";
	public static String R6 = "FVPJIAOYEDRZXWGCTKUQSBNMHL";
	public static String R7 = "KZGLIUCJEHADXRYWVTNSFQPMOB";
	public static String R8 = "ZLVGOIFTYWUEPMABNCXRQSDKHJ";
	public static String REF = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
	public static String ZAREZ = "ACEINQTVY";
	
	private Rotor DS, LS, DR, SR, LR, reflektor;
	private int[] zarezi;
	private int[] ulaz;
	int pozicija = 0;
	private int brojac = 0;
	private int izlazDS = 0, izlazLS = 0, izlazDR = 0, izlazSR = 0, izlazLR = 0, izlazDSI = 0, izlazLSI = 0, izlazDRI = 0, izlazSRI = 0,
			izlazLRI = 0, izlazReflektor = 0;
	
	public Simulator(Rotor DS, Rotor LS, Rotor DR, Rotor SR, Rotor LR, Rotor reflektor, String zarezi, String ulaz)
	{
		this.DS = DS;
		this.LS = LS;
		this.DR = DR;
		this.SR = SR;
		this.LR = LR;
		this.reflektor = reflektor;
		this.zarezi = Pretvorba.stringUArray(zarezi);
		this.ulaz = Pretvorba.stringUArray(ulaz);
	}
	
	public void korak(GlavniEkran g) throws Exception
	{
		if (pozicija >= ulaz.length)
		{
			throw new Exception();
		}
		switch (brojac)
		{
		case 0:
			for(int i = 0; i < 9; i++)
			{
				if(SR.dohvatiTrenutni() == zarezi[i])
				{
					LR.rotiraj();
					SR.rotiraj();
				}
				else if(DR.dohvatiTrenutni() == zarezi[i])
				{
					SR.rotiraj();
				}
			}
			DR.rotiraj();
			g.textFieldJedanaesti.setText("");
			g.textFieldDeseti.setText("");
			g.textFieldDeveti.setText("");
			g.textFieldOsmi.setText("");
			g.textFieldSedmi.setText("");
			g.textFieldSesti.setText("");
			g.textFieldPeti.setText("");
			g.textFieldCetvrti.setText("");
			g.textFieldTreci.setText("");
			g.textFieldDrugi.setText("");
			g.textFieldPrvi.setText("");
			
			izlazDS = DS.dohvatiKod(ulaz[pozicija]);
			g.textFieldPrvi.setText(""+ (char)(izlazDS + 'A'));
			break;
		case 1:
			izlazLS = LS.dohvatiKod(izlazDS);
			g.textFieldDrugi.setText(""+ (char)(izlazLS + 'A'));
			break;
		case 2:
			izlazDR = DR.dohvatiKod(izlazLS);
			g.textFieldTreci.setText(""+ (char)(izlazDR + 'A'));
			break;
		case 3:
			izlazSR = SR.dohvatiKod(izlazDR);
			g.textFieldCetvrti.setText(""+ (char)(izlazSR + 'A'));
			break;
		case 4:
			izlazLR = LR.dohvatiKod(izlazSR);
			g.textFieldPeti.setText(""+ (char)(izlazLR + 'A'));
			break;
		case 5:
			izlazReflektor = reflektor.dohvatiKod(izlazLR);
			g.textFieldSesti.setText(""+ (char)(izlazReflektor + 'A'));
			break;
		case 6:
			izlazLRI = LR.dohvatiInverzniKod(izlazReflektor);
			g.textFieldSedmi.setText(""+ (char)(izlazLRI + 'A'));
			break;
		case 7:
			izlazSRI = SR.dohvatiInverzniKod(izlazLRI);
			g.textFieldOsmi.setText(""+ (char)(izlazSRI + 'A'));
			break;
		case 8:
			izlazDRI = DR.dohvatiInverzniKod(izlazSRI);
			g.textFieldDeveti.setText(""+ (char)(izlazDRI + 'A'));
			break;
		case 9:
			izlazLSI = LS.dohvatiInverzniKod(izlazDRI);
			g.textFieldDeseti.setText(""+ (char)(izlazLSI + 'A'));
			break;
		case 10:
			izlazDSI = DS.dohvatiInverzniKod(izlazLSI); //kriptirano	
			g.textFieldJedanaesti.setText(""+ (char)(izlazDSI + 'A'));
			g.textFieldIzlaz.setText(g.textFieldIzlaz.getText()+""+ (char)(izlazDSI + 'A'));
			pozicija++;
			break;
		}
		brojac = (brojac + 1) % 11;
	}
	public String simulator(String ulaz)
	{
		int[] polje = Pretvorba.stringUArray(ulaz);
		int[] izlaz = new int[polje.length];
		int pozicija = 0;
		for(int slovo : polje)
		{
			for(int i = 0; i < 9; i++)
			{
				if(SR.dohvatiTrenutni() == zarezi[i])
				{
					LR.rotiraj();
					SR.rotiraj();
				}
				else if(DR.dohvatiTrenutni() == zarezi[i])
				{
					SR.rotiraj();
				}
			}
			DR.rotiraj();
			
			int izlazDS = DS.dohvatiKod(slovo);
			int izlazLS = LS.dohvatiKod(izlazDS);
			int izlazDR = DR.dohvatiKod(izlazLS);
			int izlazSR = SR.dohvatiKod(izlazDR);
			int izlazLR = LR.dohvatiKod(izlazSR);
			int izlazReflektor = reflektor.dohvatiKod(izlazLR);
			int izlazLRI = LR.dohvatiInverzniKod(izlazReflektor);
			int izlazSRI = SR.dohvatiInverzniKod(izlazLRI);
			int izlazDRI = DR.dohvatiInverzniKod(izlazSRI);
			int izlazLSI = LS.dohvatiInverzniKod(izlazDRI);
			int izlazDSI = DS.dohvatiInverzniKod(izlazLSI); //kriptirano	
			
			izlaz[pozicija] = izlazDSI;
			pozicija++;
		}
		return Pretvorba.arrayUString(izlaz);
	}

}
