package kriptografija.typex;

public class Rotor {
	
	private	int[] polje;
	private boolean unazad = false;
	private int curr=0;
	
	public Rotor(int[] polje, int pocetnoStanje, boolean unazad)
	{
		this.polje = polje;
		for(int i = 0; i < pocetnoStanje; i++)
		{
			rotiraj();
		}
		this.unazad = unazad;
	}
	
	private void idi()
	{
		int[] polje2 = new int[26];
		for(int i = 0; i < 26; i++)
		{
			polje2[i] = (polje[(i + 1) % 26] - 1 + 26) % 26;
		}
		polje = polje2;
	}
	
	public void rotiraj()
	{
		if (unazad)
		{
			curr = (curr + 25) % 26;
			for (int i = 0; i < 25; i++) 
			{
				idi();
			}
		}
		else
		{
			curr = (curr + 1) % 26;
			idi();
		}	
	}
	
	public int dohvatiKod(int ulaz)
	{
		if (unazad)
		{
			return polje[(26 - ulaz) % 26];
		}
		else
		{
			return polje[ulaz];
		}
	}
	
	public int dohvatiInverzniKod(int ulaz)
	{
		for (int i = 0; i < 26; i++)
		{
			if (polje[i] == ulaz)
			{
				if (unazad)
				{
					 return (26 - i) % 26;
				}
				return i;
			}
		}
		return -1;		
	}
	
	public int dohvatiTrenutni()
	{
		return curr;
	}
}
